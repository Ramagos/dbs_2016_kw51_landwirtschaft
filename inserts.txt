package dbs_landwirtschaft;
import java.sql.*;

public class inserts {
	
	public static void insertTier(String art, double gewicht, int geburtsjahr, String name, int chipnr) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Tiere (Art,Gewicht,Geburtsjahr,Name,Erkennungsnummer) " +
                "VALUES (\""+art+"\","+gewicht+","+geburtsjahr+",\""+name+"\","+chipnr+" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
	
	public static void insertLandwirtschaft(String name, int steuernummer, int plz, String ort, String adresse) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Landwirtschaft (Name, Steuernummer, PLZ, Ort, Adresse) " +
                "VALUES (\""+name+"\","+steuernummer+","+plz+",\""+ort+"\",\""+adresse+"\" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
	
	public static void insertStall(int id, String zugehorigkeit, int platze, int baujahr, int flache) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Stall (ID, Zugeh�rigkeit, Pl�tze, Baujahr, Fl�che) " +
                "VALUES ("+id+",\""+zugehorigkeit+"\","+platze+","+baujahr+","+flache+" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
	
	public static void insertZugangsliste(String stall, String YYYY_MM_DD, int tiernummer) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Zugangsliste (Stall, Aufnahmedatum, Tiernummer) " +
                "VALUES (\""+stall+"\","+YYYY_MM_DD+","+tiernummer+" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
	
	public static void insertBesitzer(String nachname, String vorname, int plz, String ort, String adresse) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Besitzer (Nachname, Vorname, PLZ, Ort, Adresse) " +
                "VALUES (\""+nachname+"\", \""+vorname+"\", "+plz+", \""+ort+"\", \""+adresse+"\" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
	
	public static void insertKauf(String kaufer, String YYYY_MM_DD, int tiernummer) throws SQLException
	{
		main.stmt = main.c.createStatement();
		main.sql = "INSERT INTO Kauf (K�ufer,Kaufdatum,Tiernummer) " +
                "VALUES (\""+kaufer+"\", "+YYYY_MM_DD+", "+tiernummer+" );"; 
		System.out.println(main.sql);
		main.stmt.executeUpdate(main.sql);
		System.out.println("inserted successfully");
		main.stmt.close();
	}
}